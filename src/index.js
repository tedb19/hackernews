const { GraphQLServer } = require('graphql-yoga')
const { Prisma } = require('prisma-binding')
const AuthPayload = require('./resolvers/AuthPayload')
const Mutation = require('./resolvers/Mutation')
const Query = require('./resolvers/Query')
const Subscription = require('./resolvers/Subscription')
const Feed = require('./resolvers/Feed')

const resolvers = {
  Query,
  Mutation,
  AuthPayload,
  Subscription,
  Feed
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  resolverValidationOptions: {
    requireResolversForResolveType: false
  },
  context: req => ({
    ...req,
    db: new Prisma({
      typeDefs: 'src/generated/prisma.graphql',
      endpoint: 'https://eu1.prisma.sh/teddy-odhiambo-550087/database/dev',
      secret: 'mysecret123',
      debug: true
    })
  })
})

server.start(() => console.log(`server is running on http://localhost:4000`))
